CREATE TABLE [dbo].[Contact]
(
[id] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CRD__c] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FINRA_Status__c] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Prior_Firm_Name__c] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Broker_Dealer__c] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FINRA_Detailed_Report_URL__c] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BD_Date_of_Hire__c] [date] NULL,
[Date_Became_Rep__c] [date] NULL,
[RIA_Firm__c] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BD_Firm__c] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Dually_Registered__c] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Licenses_Held__c] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
